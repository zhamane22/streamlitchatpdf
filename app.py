from PyPDF2 import PdfReader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores import FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.chat_models import ChatOpenAI
from langchain.chains import ConversationalRetrievalChain
import pickle
from pathlib import Path
from dotenv import load_dotenv
import os
import streamlit as st
from streamlit_chat import message
import io
import asyncio
import glob

load_dotenv()
api_key = os.getenv('OPENAI_API_KEY')


async def storeDocEmbeds(file_paths: list[str], output_name: str) -> None:
    all_chunks = []
    for file_path in file_paths:
        with open(file_path, "rb") as f:
            file_content = f.read()

        reader = PdfReader(io.BytesIO(file_content))
        corpus = ''.join([p.extract_text() for p in reader.pages if p.extract_text()])
        splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200)
        chunks = splitter.split_text(corpus)
        all_chunks.extend(chunks)

        with open(f"data/txt/{output_name}.txt", "a") as f:
            f.write(f"{file_path}\n________________\n")
            f.write("\n".join(chunks))
            f.write("\n\n")

    embeddings = OpenAIEmbeddings(openai_api_key=api_key)
    vectors = FAISS.from_texts(all_chunks, embeddings)

    with open(f"data/pkl/{output_name}.pkl", "wb") as f:
        pickle.dump(vectors, f)

async def getDocEmbeds(file_paths: list[str], output_name: str) -> FAISS:
    if not os.path.isfile(f"data/pkl/{output_name}.pkl"):
        await storeDocEmbeds(file_paths, output_name)

    with open(f"data/pkl/{output_name}.pkl", "rb") as f:
        global vectores
        vectors = pickle.load(f)

    return vectors

async def conversational_chat(query: str, qa: ConversationalRetrievalChain) -> str:
    result = qa({"question": query, "chat_history": st.session_state['history']})
    st.session_state['history'].append((query, result["answer"]))
    return result["answer"]

async def main():
    llm = ChatOpenAI(model_name="gpt-3.5-turbo")
    chain = load_qa_chain(llm, chain_type="stuff")

    if 'history' not in st.session_state:
        st.session_state['history'] = []

    st.image("logo.png")

    if 'ready' not in st.session_state:
        st.session_state['ready'] = False

    pdf_files = glob.glob("data/pdf/*.pdf")

    if not pdf_files:
        st.warning("No PDF files found in 'data/pdf' directory.")
        return

    output_name = "combined"

    st.divider()

    if pdf_files is not None:
        with st.spinner("Processing..."):
            vectors = await getDocEmbeds(pdf_files, output_name)
            qa = ConversationalRetrievalChain.from_llm(ChatOpenAI(model_name="gpt-3.5-turbo"),
                                                       retriever=vectors.as_retriever(), return_source_documents=True)

        st.session_state['ready'] = True

    if st.session_state['ready']:

        if 'generated' not in st.session_state:
            st.session_state['generated'] = ["Welcome! You can now ask any questions regarding the combined PDF files."]
    if 'past' not in st.session_state:
        st.session_state['past'] = ["Hey!"]

    # container for chat history
    response_container = st.container()

    # container for text box
    container = st.container()

    with container:
        with st.form(key='my_form', clear_on_submit=True):
            user_input = st.text_input("Query:", placeholder="e.g: Summarize the papers in a few sentences",
                                       key='input')
            submit_button = st.form_submit_button(label='Send')

        if submit_button and user_input:
            output = await conversational_chat(user_input, qa)
            st.session_state['past'].append(user_input)
            st.session_state['generated'].append(output)

    if st.session_state['generated']:
        with response_container:
            for i in range(len(st.session_state['generated'])):
                message(st.session_state["past"][i], is_user=True, key=str(i) + '_user', avatar_style="thumbs")
                message(st.session_state["generated"][i], key=str(i), avatar_style="fun-emoji")


if __name__ == "__main__":
    asyncio.run(main())
