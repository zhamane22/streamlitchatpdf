import requests
from bs4 import BeautifulSoup
import os
import urllib.parse

    url = 'https://www.ecologie.gouv.fr/operations-standardisees-deconomies-denergie'

def download_pdf_file(pdf_url, save_path):
    response = requests.get(pdf_url)
    with open(save_path, 'wb') as pdf_file:
        pdf_file.write(response.content)

def download_all_pdfs(url, save_folder='data/pdf'):
    os.makedirs(save_folder, exist_ok=True)
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')

    for link in soup.find_all('a', href=True):
        href = link['href']
        if href.endswith('.pdf'):
            pdf_url = href
            if not pdf_url.startswith('http'):
                pdf_url = 'https://www.ecologie.gouv.fr' + pdf_url
            file_name = pdf_url.split('/')[-1]
            # Decode special characters
            file_name = urllib.parse.unquote(file_name)
            save_path = os.path.join(save_folder, file_name)
            download_pdf_file(pdf_url, save_path)
            print(f'Downloaded: {file_name}')

if __name__ == '__main__':
    download_all_pdfs(url)
